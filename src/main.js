
var lightState = "red";
var $togglebtn = $("#toggle-btn");
var flashStatus = false;
var flashIntervalID;
var updateIntervalID;

drawLights();

webSocketConnect();



function webSocketConnect(){
    const exampleSocket = new WebSocket(
        "wss://mqtt.zhtc.nl:1880/stoplicht/status"
      );
      console.log("connecting...");

      exampleSocket.onopen = (event) => {
        console.log("connected !!");
        updateIntervalID = setInterval(function() {
            exampleSocket.send("state");
        }, 1000);
        
      };
      
      exampleSocket.onmessage = (event) => {
        lightState = event.data.toString();
        console.log("status: " + lightState);
        drawLights();
      };

      exampleSocket.onclose = (event) => {
        clearInterval(updateIntervalID)
        console.log("Disconnected :(");
      };
      
}

    

function drawLights() {
    switch (lightState) {
        case "red":
            clearInterval(flashIntervalID);
            setGreenLight(false);
            setRedLight(true);
            break;
            case "green":
                clearInterval(flashIntervalID);
                setGreenLight(true);
                setRedLight(false);
            break;
            case "flash":
            flashIntervalID = setInterval(flashHandler,500);
            break;
    
        default:
            clearInterval(flashIntervalID);
            setGreenLight(false);
            setRedLight(false);
            break;
    }
}

function setRedLight(state) {
    $light = $("#redLight");
    if (state) {
        $light.removeClass('lightOff');
        $light.addClass('lightOn');
        
    }
    else
    {
        $light.removeClass('lightOn');
        $light.addClass('lightOff');
    }
}

function setGreenLight(state) {
    $light = $("#greenLight");
    if (state) {
        $light.removeClass('lightOff');
        $light.addClass('lightOn');
        
    }
    else
    {
        $light.removeClass('lightOn');
        $light.addClass('lightOff');
    }
}

function flashHandler() {    
    if (flashStatus) {
        setGreenLight(true);
                setRedLight(false);
    }
    else
    {
        setGreenLight(false);
            setRedLight(true);
    }
    flashStatus = !flashStatus;
}
